import AlexaError from '@twbdrd/alexa-skill-type/source/SmartHome/Enumeration/Error/Alexa';
import CookingError from '@twbdrd/alexa-skill-type/source/SmartHome/Enumeration/Error/Cooking';
import VideoError from '@twbdrd/alexa-skill-type/source/SmartHome/Enumeration/Error/Video';
import ResponseName from '@twbdrd/alexa-skill-type/source/SmartHome/Enumeration/ResponseName';
import Endpoint from '@twbdrd/alexa-skill-type/source/SmartHome/Type/Endpoint';
import Header from '@twbdrd/alexa-skill-type/source/SmartHome/Type/Header';
import SkillRequest from '@twbdrd/alexa-skill-type/source/SmartHome/Type/SkillRequest';
import Base from './Base';

type ErrorType = AlexaError | CookingError | VideoError;

export interface ErrorPayload {
  message: string;
  type: ErrorType;

  [index: string]: any;
}

export default abstract class Error extends Base {
  protected endpoint: Endpoint | undefined;
  protected header: Header;

  // noinspection TypeScriptAbstractClassConstructorCanBeMadeProtected
  constructor(event: SkillRequest) {
    super();
    const directive = event.directive;
    this.header = this.clone(directive.header);
    this.header.name = ResponseName.Error;
    this.endpoint = this.clone(directive.endpoint);
  }

  protected makeError(payload: ErrorPayload) {
    const endpoint = this.endpoint;
    const item = {
      event: {
        header: this.header,
        payload
      }
    };

    if (undefined !== endpoint) {
      (<any>item).endpoint = endpoint;
    }

    return item;
  }

}

