import Base from './Base/Base';
import Skill from './Skill';

export default class Provider extends Base {
  readonly skill: Skill;

  constructor(skill: Skill) {
    super();
    this.skill = skill;
  }

}
